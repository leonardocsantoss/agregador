# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models

from suit.widgets import SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget, LinkedSelect, AutosizedTextarea
from suit_ckeditor.widgets import CKEditorWidget

from models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', '_actions', )
    search_fields = ('name',)
    save_on_top = True

    fieldsets = [
        (None, {'fields': ('name', ), }, ),
    ]
admin.site.register(Category, CategoryAdmin)


class FeedAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', '_actions', )
    search_fields = ('name', 'url',)
    save_on_top = True

    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget(editor_options={'lang': 'pt', "toolbarGroups": [{"name": "basicstyles", "groups": ["basicstyles", "cleanup"]}, {"name": "paragraph", "groups": ["list", "align"]}, {"name": "styles"}, {"name": "colors"}, {"items": ["Image", "Flash", "Table", "HorizontalRule"], "name": "insert_custom"}, ], "autoGrow_maxHeight": 250, "autoGrow_onStartup": True, "autoGrow_minHeight": 100, "extraPlugins": "autogrow"}),},
    }
    fieldsets = [
        (None, {'fields': ('name', 'url',), }, ),
        (u'Descrição', {'fields': ('description', ), }, ),
    ]
admin.site.register(Feed, FeedAdmin)


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'feed', 'pubdate', '_actions', )
    search_fields = ('title',)
    list_filter = ('feed', 'category', 'pubdate')
    save_on_top = True
    formfield_overrides = {
        models.DateTimeField: {'widget': SuitSplitDateTimeWidget },
        models.TextField: {'widget': CKEditorWidget(editor_options={'lang': 'pt', "toolbarGroups": [{"name": "basicstyles", "groups": ["basicstyles", "cleanup"]}, {"name": "paragraph", "groups": ["list", "align"]}, {"name": "styles"}, {"name": "colors"}, {"items": ["Image", "Flash", "Table", "HorizontalRule"], "name": "insert_custom"}, ], "autoGrow_maxHeight": 250, "autoGrow_onStartup": True, "autoGrow_minHeight": 100, "extraPlugins": "autogrow"}),},
        models.ForeignKey: {'widget': LinkedSelect },
    }

    fieldsets = [
        (u'Feed', {'fields': ('feed', 'feedid',), }, ),
        (u'Notícia', {'fields': ('title', 'link', 'author', 'pubdate'), }, ),
        (u'Descrição', {'fields': ('description', ), }, ),
        (u'Categoria', {'fields': ('category', ), }, ),
    ]
admin.site.register(News, NewsAdmin)
