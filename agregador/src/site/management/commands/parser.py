# -*- coding: utf-8 -*-
from django.core.management.base import NoArgsCommand

from agregador.src.site.models import Feed


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        for feed in Feed.objects.all():
            feed.feed_to_news()