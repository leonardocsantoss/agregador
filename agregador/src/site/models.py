#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.db.models import signals
from django.conf import settings

from bs4 import BeautifulSoup
from datetime import datetime
import feedparser, urllib2


class Category(models.Model):
    class Meta:
        ordering = ('name', )
        verbose_name = u"Categoria"
        verbose_name_plural = u"Categorias"

    name = models.CharField(u'Nome', max_length=120)

    def __unicode__(self):
        return u'%s' % self.name

    def _actions(self):
        acoes = u'<div class="col-actions" style="width: 77px;">'
        acoes += u'<a href="%s" class="icon-edit" alt="%s"></a>' % (self.pk, u"Editar")
        if hasattr(self, 'get_absolute_url'):
            acoes += u'<a href="%s" class="icon-eye-open" alt="%s"></a>' % (self.get_absolute_url(), u"Ver no site")
        acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" alt="%s"></a>' % (self.pk, u"Remover")
        acoes += u'</div>'
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"


class Feed(models.Model):
    class Meta:
        ordering = ('id', )
        verbose_name = u"Feed"
        verbose_name_plural = u"Feeds"

    name = models.CharField(u'Nome', max_length=120)
    url = models.URLField(u'Url')
    description = models.TextField(u'Descrição', blank=True, null=True)

    def have_equal_news(self, title):
        import difflib
        for news in News.objects.values_list('title', flat=True):
            if difflib.SequenceMatcher(None, title, news).ratio() > 0.7:
                return True
        return False

    def get_or_create_news(self, item):
        try:
            news = News.objects.get(
                feed=self,
                feedid=item.get('link')
            )
            return news, False
        except News.DoesNotExist:
            if not self.have_equal_news(item.get('title')):
                news = News.objects.create(
                    feed=self,
                    feedid=item.get('link'),
                    title=item.get('title'),
                    link=item.get('link'),
                    description=item.get('description', ''),
                    author=item.get('author', ''),
                    pubdate=datetime.now(),
                )
                if item.get('category'):
                    category = Category.objects.get_or_create(name=item.get('category').strip())[0]
                    news.category = category
                    news.save()
                return news, True
            return None, True

    def feed_to_news(self):
        parse = feedparser.parse(self.url)
        for item in parse['items']:
            self.get_or_create_news(item)

    def __unicode__(self):
        return self.url

    def _actions(self):
        acoes = u'<div class="col-actions" style="width: 77px;">'
        acoes += u'<a href="%s" class="icon-edit" alt="%s"></a>' % (self.pk, u"Editar")
        if hasattr(self, 'get_absolute_url'):
            acoes += u'<a href="%s" class="icon-eye-open" alt="%s"></a>' % (self.get_absolute_url(), u"Ver no site")
        acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" alt="%s"></a>' % (self.pk, u"Remover")
        acoes += u'</div>'
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"


class News(models.Model):
    class Meta:
        ordering = ('-pubdate', )
        verbose_name = u"Notícia"
        verbose_name_plural = u"Notícias"
        unique_together = (("feed", "feedid"),)

    feed = models.ForeignKey(Feed)
    feedid = models.CharField(u'Feed id', max_length=255)
    title = models.CharField(u"Título", max_length=255)
    link = models.URLField(u"Link", blank=True, null=True)
    description = models.TextField(u'Descrição', blank=True, null=True)
    author = models.CharField(u"Autor", max_length=255, blank=True, null=True)
    pubdate = models.DateTimeField(u"Data de publicação", default=datetime.now)
    category = models.ForeignKey(Category, verbose_name=u"Categorias", blank=True, null=True)

    def __unicode__(self):
        return u"%s" % (self.title, )

    def _actions(self):
        acoes = u'<div class="col-actions" style="width: 77px;">'
        acoes += u'<a href="%s" class="icon-edit" alt="%s"></a>' % (self.pk, u"Editar")
        if hasattr(self, 'get_absolute_url'):
            acoes += u'<a href="%s" class="icon-eye-open" alt="%s"></a>' % (self.get_absolute_url(), u"Ver no site")
        acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" alt="%s"></a>' % (self.pk, u"Remover")
        acoes += u'</div>'
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"