(function($) {
    $(document).ready(function() {
        
        function showAddress(address, zoom) {
            geocoder.geocode({'address': address}, function(results, status){
                if(status == google.maps.GeocoderStatus.OK){
                    latlng = results[0].geometry.location;
                    marker.setOptions({position: latlng, map: map});
                    $('#id_latitude').val(latlng.lat());
                    $('#id_longitude').val(latlng.lng());
                    map.setCenter(latlng);
                    map.setZoom(zoom);
                }
            });
        }
        
        var mapDiv = document.getElementById('map-canvas');
        var map = new google.maps.Map(mapDiv, {
          center: new google.maps.LatLng($('#id_latitude').val(), $('#id_longitude').val()),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker({
          map: map,
          position: new google.maps.LatLng($('#id_latitude').val(), $('#id_longitude').val()),
          draggable: true
        });
        
        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(event) {
          $('#id_latitude').val(event.latLng.lat());
          $('#id_longitude').val(event.latLng.lng());
        });
        
        $('.latitude, .longitude').hide();
        
        $('#id_localizacao').keyup(function(){
            showAddress($('#id_localizacao').val(), 14);
        });
        
    });
})(django.jQuery);