from __future__ import with_statement
from fabric.api import *

env.hosts = [
    'root@agregador.in4.com.br',
]
env.warn_only = True

def deploy():
    with cd('/deploy/agregador.in4.com.br/agregador'):
        # get lastest version from git
        run('git pull')
        # restart nginx
        run('supervisorctl restart agregador')


def deploy_migrate():
    with cd('/deploy/agregador.in4.com.br/agregador'):
        # get lastest version from git
        run('git pull')
        run('../bin/python manage.py syncdb --settings=agregador.settings_production')
        run('../bin/python manage.py migrate --settings=agregador.settings_production')
        # restart nginx
        run('supervisorctl restart agregador')